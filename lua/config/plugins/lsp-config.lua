local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status then
	return
end

local cmp_nvim_lsp_status, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not cmp_nvim_lsp_status then
	return
end

local mason_status, mason = pcall(require, "mason")
if not mason_status then
	return
end

local mason_lspconfig_status, mason_lspconfig = pcall(require, "mason-lspconfig")
if not mason_lspconfig_status then
	return
end

local mason_null_ls_status, mason_null_ls = pcall(require, "mason-null-ls")
if not mason_null_ls_status then
	return
end

local keymap = vim.keymap

local servers = {
	"lua_ls",
	"tailwindcss",
	"svelte",
	"tsserver",
	"eslint",
	"html",
	"bashls",
	"cssls",
	"angularls",
	"jdtls",
	"astro",
	"clangd",
	"rust_analyzer",
}

mason.setup()

mason_lspconfig.setup({
	ensure_installed = servers,
	automatic_installation = true,
})

mason_null_ls.setup({
	ensure_installed = {
		"shellcheck",
		"beautysh",
		"prettier",
		"google-java-format",
		"clang-format",
		"stylua",
	},
	automatic_installation = false,
	handlers = {},
	automatic_setup = true,
})

local on_attach = function(client, bufnr)
	--[[ if client.name == "tsserver" or client.name == "svelte" then ]]
	--[[ 	local ns = vim.lsp.diagnostic.get_namespace(client.id) ]]
	--[[ 	vim.diagnostic.disable(nil, ns) ]]
	--[[ end ]]

	local opts = { noremap = true, silent = true, buffer = bufnr }

	keymap.set("n", "gf", "<cmd>Lspsaga lsp_finder<CR>", opts) -- show definition, references
	keymap.set("n", "gD", "<Cmd>lua vim.lsp.buf.declaration()<CR>", opts) -- got to declaration
	keymap.set("n", "gd", "<cmd>Lspsaga peek_definition<CR>", opts) -- see definition and make edits in window
	keymap.set("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts) -- go to implementation
	keymap.set("n", "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts) -- see available code actions
	keymap.set("n", "<leader>rn", "<cmd>Lspsaga rename<CR>", opts) -- smart rename
	keymap.set("n", "<leader>d", "<cmd>Lspsaga show_line_diagnostics<CR>", opts) -- show  diagnostics for line
	keymap.set("n", "<leader>d", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts) -- show diagnostics for cursor
	keymap.set("n", "[d", "<cmd>Lspsaga diagnostic_jump_prev<CR>", opts) -- jump to previous diagnostic in buffer
	keymap.set("n", "]d", "<cmd>Lspsaga diagnostic_jump_next<CR>", opts) -- jump to next diagnostic in buffer
	keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts) -- show documentation for what is under cursor
	keymap.set("n", "<leader>o", "<cmd>LSoutlineToggle<CR>", opts) -- see outline on right hand side
end

local capabilities = cmp_nvim_lsp.default_capabilities() -- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers

for _, server in pairs(servers) do
	if server == "lua_ls" then
		lspconfig[server].setup({
			on_attach = on_attach,
			capabilities = capabilities,
			settings = { -- custom settings for lua
				Lua = {
					-- make the language server recognize "vim" global
					diagnostics = {
						globals = { "vim" },
					},
					workspace = {
						-- make language server aware of runtime files
						library = {
							[vim.fn.expand("$VIMRUNTIME/lua")] = true,
							[vim.fn.stdpath("config") .. "/lua"] = true,
						},
					},
				},
			},
		})
	else
		lspconfig[server].setup({
			on_attach = on_attach,
			capabilities = capabilities,
		})
	end
end

require("tailwind-sorter").setup({
	on_save_pattern = { "*.html", "*.jsx", "*.tsx" },
})
