local status, lualine = pcall(require, "lualine")
if not status then
	return
end

lualine.setup({
	options = {
		theme = "gruvbox-baby",
		--[[ section_separators = { left = "", right = "" }, ]]
		--[[ component_separators = { left = "", right = "" }, ]]
	},
})

-- add nvim-tree support option later on
