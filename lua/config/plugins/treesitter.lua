local status, treesitter = pcall(require, "nvim-treesitter.configs")
if not status then
	return
end

treesitter.setup({
	ensure_installed = {
		"bash",
		"css",
		"gitignore",
		"html",
		"javascript",
		"json",
		"lua",
		"regex",
		"svelte",
		"vim",
		"java",
		"markdown",
		"markdown_inline",
		"astro",
		"tsx",
		"typescript",
		"rust",
	},
	auto_install = false,
	indent = {
		enable = true,
	},
	autotag = {
		enable = true,
	},
	highlight = {
		enable = true,
	},
})
