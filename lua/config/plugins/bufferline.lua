local bufferline_ok, bufferline = pcall(require, "bufferline")
if not bufferline_ok then
	return
end

bufferline.setup({
	options = {
		--[[ buffer_close_icon = "", ]]
		buffer_close_icon = "󰱝",
		separator_style = "thin",
		diagnostics = "nvim_lsp",
		offsets = {
			{
				filetype = "NvimTree",
				text = "File Explorer",
				text_align = "center",
				separator = true,
			},
		},
	},
})
