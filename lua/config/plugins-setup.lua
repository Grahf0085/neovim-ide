local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = " "

local status, lazyNvim = pcall(require, "lazy")
if not status then
	return
end

return lazyNvim.setup({
	{ "luisiacc/gruvbox-baby", branch = "main" },
	"navarasu/onedark.nvim",
	{
		"AlexvZyl/nordic.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			require("nordic").load()
		end,
	},
	"sainnhe/sonokai",
	"sainnhe/edge",
	{ "Everblush/nvim", name = "everblush" },
	"sainnhe/gruvbox-material",
	"rmehri01/onenord.nvim",
	"EdenEast/nightfox.nvim",
	"numToStr/Comment.nvim", -- comment out code
	"nvim-lua/plenary.nvim", -- lua functions that many plugins use
	{
		"nvim-tree/nvim-tree.lua", -- file explorer
		dependencies = {
			"nvim-tree/nvim-web-devicons", -- optional, for file icons
		},
		version = "nightly", -- optional, updated every week. (see issue #1193)
	},
	{
		"nvim-lualine/lualine.nvim", -- thing at bottom of screen
		dependencies = { "nvim-tree/nvim-web-devicons", lazy = true },
	},
	{
		"nvim-telescope/telescope.nvim",
		tag = "0.1.4",
		dependencies = { "nvim-lua/plenary.nvim" },
	},
	{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" }, -- for better sorting performance
	"hrsh7th/nvim-cmp", -- autocompletion plugin
	"hrsh7th/cmp-buffer", -- source for text in buffer
	"hrsh7th/cmp-path", -- source for file system paths
	"hrsh7th/cmp-cmdline", -- vim command lind completion source
	{
		"L3MON4D3/LuaSnip",
		-- follow latest release.
		version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
		-- install jsregexp (optional!).
		build = "make install_jsregexp",
	},
	"saadparwaiz1/cmp_luasnip", -- for autocompletion
	"rafamadriz/friendly-snippets", -- useful snippets
	"williamboman/mason.nvim", -- in charge of managing lsp servers, linters & formatters
	"williamboman/mason-lspconfig.nvim", -- bridges gap b/w mason & lspconfig
	"neovim/nvim-lspconfig", -- easily configure language servers
	"hrsh7th/cmp-nvim-lsp", -- for autocompletion
	{ "glepnir/lspsaga.nvim", event = "BufRead" }, -- enhanced lsp uis
	"onsails/lspkind.nvim", -- vs-code like icons for autocompletion
	"nvimtools/none-ls.nvim", -- configure formatters & linters
	{
		"jay-babu/mason-null-ls.nvim",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = {
			"williamboman/mason.nvim",
			"nvimtools/none-ls.nvim",
		},
	},
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
	},
	"windwp/nvim-autopairs", -- autoclose parens, brackets, quotes, etc...
	"windwp/nvim-ts-autotag", -- autoclose and rename tags
	"lewis6991/gitsigns.nvim", -- indicate code modifications
	"JoosepAlviste/nvim-ts-context-commentstring", -- jsx comments
	{ "akinsho/bufferline.nvim", version = "*", dependencies = "nvim-tree/nvim-web-devicons" }, -- tabs
	"hinell/move.nvim", -- move multiple lines of code
	"solidjs-community/solid-snippets",
	"mfussenegger/nvim-jdtls",
	"mfussenegger/nvim-dap",
	{
		"roobert/tailwindcss-colorizer-cmp.nvim",
		config = function()
			require("tailwindcss-colorizer-cmp").setup({
				color_square_width = 2,
			})
		end,
	},
	{
		"laytan/tailwind-sorter.nvim",
		dependencies = { "nvim-treesitter/nvim-treesitter", "nvim-lua/plenary.nvim" },
		build = "cd formatter && npm i && npm run build",
		config = function()
			require("tailwind-sorter").setup({
				on_save_pattern = { "*.html", "*.jsx", "*.tsx" },
			})
		end,
	},
})

-- do I want to use cmp-nvim-lua?
