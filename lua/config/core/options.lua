local opt = vim.opt

-- line numbers
opt.number = true

-- tabs and indents
opt.tabstop = 2 -- 2 spaces for tabs
opt.shiftwidth = 2 -- number of spaces inserted for each indentation
opt.expandtab = true -- convert tabs to spaces
opt.smartindent = true -- copy indent from current line when starting new one

-- line wrapping
opt.wrap = false

-- search settings
opt.ignorecase = true -- ignore case when searching
opt.smartcase = true -- if you include mixed case in your search, assumes you want case-sensitive

--appearance
opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "yes" -- show sign column so that text doesn't shift
opt.syntax = "ON"
--[[ opt.cmdheight = 2 -- more spce in command line for displaying messages ]]

-- backspace
opt.backspace = "indent,eol,start" -- allow backspace on indent, end of line or insert mode start position

-- clipboard
opt.clipboard:append("unnamedplus") -- use system clipboard as default register

-- split windows
opt.splitright = true
opt.splitbelow = true

-- file types
opt.encoding = "utf-8"
vim.scriptencoding = "utf-8"
opt.fileencoding = "utf-8"

-- files
opt.backup = false
opt.writebackup = false -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
opt.swapfile = false

opt.iskeyword:append("-") -- consider string-string as whole word

vim.cmd([[au BufEnter * set fo-=c fo-=r fo-=o]]) -- Stop new line comments

vim.g.skip_ts_context_commentstring_module = true -- speed up loading
