--vim.g.mapleader = " "  -- map leader defined in plugins-setup.lua

local keymap = vim.keymap

-- [[ general ]]
keymap.set("i", "jk", "<ESC>") -- use jk to exit insert mode
keymap.set("n", "<leader>nh", ":nohl<CR>") -- clear search highlights
keymap.set("n", "x", '"_x') -- delete single character without copying into register

keymap.set("n", "<leader>=", "<C-a>") -- increment number
keymap.set("n", "<leader>-", "<C-x>") -- decrement number

keymap.set("n", "<leader>sv", "<C-w>v") -- split window vertically
keymap.set("n", "<leader>sh", "<C-w>s") -- split window horizontally
keymap.set("n", "<leader>se", "<C-w>=") -- make split windows equal width & height
keymap.set("n", "<leader>sx", ":close<CR>") -- close current split window

keymap.set("n", "<c-j>", "<c-w>j") -- move to split below
keymap.set("n", "<c-h>", "<c-w>h") -- move to split on left
keymap.set("n", "<c-k>", "<c-w>k") -- move to split above
keymap.set("n", "<c-l>", "<c-w>l") -- move to split on right

keymap.set("n", "<leader>to", ":tabnew<CR>") -- open new tab
keymap.set("n", "<leader>tx", ":tabclose<CR>") -- close current tab
keymap.set("n", "<leader>tn", ":tabn<CR>") --  go to next tab
keymap.set("n", "<leader>tp", ":tabp<CR>") --  go to previous tab

-- [[ plugins ]]

-- telescope
keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<cr>") -- find files within current working directory, respects .gitignore
keymap.set("n", "<leader>fs", "<cmd>Telescope live_grep<cr>") -- find string in current working directory as you type
keymap.set("n", "<leader>fc", "<cmd>Telescope grep_string<cr>") -- find string under cursor in current working directory
keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>") -- list open buffers in current neovim instance
keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>") -- list available help tags
-- gr for reference

-- Saving
keymap.set("n", "<c-s>", ":w<cr>")
keymap.set("i", "<c-s>", "<Esc>:w<cr>a", {})

-- Naviagate buffers
keymap.set("n", "<S-l>", ":bnext<CR>")
keymap.set("n", "<S-h>", ":bprevious<CR>")

-- Move Normal-mode commands
vim.keymap.set("n", "<A-j>", ":MoveLine 1<CR>")
vim.keymap.set("n", "<A-k>", ":MoveLine -1<CR>")
vim.keymap.set("n", "<A-h>", ":MoveHChar -1<CR>")
vim.keymap.set("n", "<A-l>", ":MoveHChar 1<CR>")

-- Visual-mode commands
vim.keymap.set("x", "<A-j>", ":MoveBlock 1<CR>")
vim.keymap.set("x", "<A-k>", ":MoveBlock -1<CR>")
vim.keymap.set("v", "<A-h>", ":MoveHBlock -1<CR>")
vim.keymap.set("v", "<A-l>", ":MoveHBlock 1<CR>")
