--[[ vim.g.gruvbox_material_better_performance = 1 ]]
--[[ vim.g.gruvbox_material_background = "soft" ]]

--[[ vim.g.edge_style = "aura" ]]
--[[ vim.g.edge_better_performance = 1 ]]
--[[ vim.g.edge_dim_forground = 1 ]]

-- Enable telescope theme
--[[ vim.g.gruvbox_baby_telescope_theme = 1 ]]
--[[ vim.g.gruvbox_baby_background_color = "soft" ]]

local status, _ = pcall(vim.cmd, "colorscheme nordfox")
if not status then
	print("Colorscheme not found")
	return
end

--[[ require("onedark").setup({ ]]
--[[ 	style = "warm", ]]
--[[ }) ]]
--[[ require("onedark").load() ]]
